        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1><?= ucwords($judul) ?></h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active"><?= ucwords($judul) ?></li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">DataTable with default features</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Email</th>
                                            <th>Peran</th>
                                            <th>Aktif</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php  
                                            foreach ($akun as $key => $value) {
                                                if ($value->peran == 1) {
                                                    $peran = '<button type="button" class="btn btn-info btn-sm">Administrator</button>';
                                                } elseif ($value->peran == 2) {
                                                    $peran = '<button type="button" class="btn btn-info btn-sm">Dosen</button>';
                                                } elseif ($value->peran == 3) {
                                                    $peran = '<button type="button" class="btn btn-info btn-sm">Mahasiswa</button>';
                                                }
                                                

                                                if ($value->aktif == "1") {
                                                    $aktif = '<button type="button" class="btn btn-outline-success btn-sm">Terverifikasi</button>';
                                                } else {
                                                    $aktif = '<button type="button" class="btn btn-outline-danger btn-sm">Belum Terverifikasi</button>';
                                                }                                                
                                        ?>
                                        <tr>
                                            <td><?php echo $value->id_user ?></td>
                                            <td><?php echo $value->email ?></td>
                                            <td class="text-center"><?php echo $peran ?></td>
                                            <td class="text-center"><?php echo $aktif ?></td>
                                            <td class="text-center">
                                                <button type="button" class="btn btn-outline-warning btn-sm" data-toggle="modal" data-target="#myModal<?= $value->id_user ?>">Ubah</button>
                                                <button type="button" class="btn btn-outline-danger btn-sm">Hapus</button>
                                            </td>
                                        </tr>

                                        <!-- Modal -->
                                        <div class="modal fade" id="myModal<?= $value->id_user ?>" role="dialog">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Ubah data <?= ucwords($judul) ?></h4>
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>This is a small modal.</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php 
                                            } 
                                        ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Email</th>
                                            <th>Peran</th>
                                            <th>Aktif</th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>