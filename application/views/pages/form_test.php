<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	User
	<table border="1">
		<thead>
			<tr>
				<th>UID</th>
				<th>email</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($the_user as $key => $val_u): ?>
				<tr>
					<td><?php echo $val_u->id_user ?></td>
					<td><?php echo $val_u->email ?></td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>

	Dosen
	<table border="1">
		<thead>
			<tr>
				<th>NIP</th>
				<th>Nama</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($dosen as $key => $val_d): ?>
				<tr>
					<td><?php echo $val_d->NIP ?></td>
					<td><?php echo $val_d->nama ?></td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>

	Mahasiswa
	<table border="1">
		<thead>
			<tr>
				<th>NIM</th>
				<th>Nama</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($mahasiswa as $key => $val_m): ?>
				<tr>
					<td><?php echo $val_m->NIM ?></td>
					<td><?php echo $val_m->nama ?></td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>

	<form method="POST" action="<?php echo base_url('test/insert_db');?>" enctype="multipart/form-data">
		<input type="file" name="file">
		<button type="submit">SAVE</button>
	</form>
</body>
</html>