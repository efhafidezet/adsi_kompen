<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_user extends CI_Model{

    public function __construct()
    {
        parent::__construct();
    }

    public function get_data_user($table, $condition)
    {
        if($table != "" && $condition != ""){
            return $this->db->get_where($table, $condition);
        }else if($table != "" && $condition == ""){
            return $this->db->get($table);
        }else {
            return FALSE;
        }
    }
}