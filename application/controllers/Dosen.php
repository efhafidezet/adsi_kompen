<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['dosen'] = $this->Dashboard_m->selectAll('user_dosen');
        $data['judul'] = "dosen";
		$this->template->set('title', 'dosen');
        $this->template->load('template', 'pages/dosen', $data);
    }
}