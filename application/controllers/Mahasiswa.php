<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['mahasiswa'] = $this->Dashboard_m->selectAll('user_mahasiswa');
        $data['judul'] = "Mahasiswa";
		$this->template->set('title', 'mahasiswa');
        $this->template->load('template', 'pages/mahasiswa', $data);
    }
}