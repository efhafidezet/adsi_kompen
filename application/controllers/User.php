<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }

    function login()
    {
    	$this->load->view('pages/form_login');
    }

    function auth()
    {
        $email = $this->input->post('email');
	    $password = $this->input->post('password');	

	    $postcontent = array(
	        'email' => $email,
	        'kodePass' => md5($password)
	    );

	    $authProc = $this->M_user->get_data_user('the_user', $postcontent);

	    if($authProc->num_rows() > 0){
            foreach($authProc->result() as $data){
            	if ($data->aktif == 1) {
            		$post_user = array(
				        'id_user' => $data->id_user
				    );

            		if ($data->peran == 1) {		# code... ADMIN
            			# code... ADMIN
            		} elseif ($data->peran == 2) {	# code... DOSEN
					    $data_the_user = $this->M_user->get_data_user('user_dosen', $post_user)->result();

                        $data_session   = array(
                            'nama' => $data_the_user->nama
                        );

                        $this->session->set_userdata($data_session);

                        redirect(base_url().'dosen');
            		} elseif ($data->peran == 3) {	# code... MAHASISWA
					    $data_the_user = $this->M_user->get_data_user('user_mahasiswa', $post_user)->result();

                        $data_session   = array(
                            'nama' => $data_the_user[0]->nama
                        );

                        $this->session->set_userdata($data_session);

                        redirect(base_url().'mahasiswa');
            		} else {
            			echo "berhasil login. akun verif. undefined";
            		}     
            		// print_r($data_the_user);
            	} else {
            		echo "berhasil login. akun belum  verif";
            	}
            	
                // $data_session 	= array(
                // 	'' => $data->email
                // );
                
                // $this->session->set_userdata($data_session);
            }

            
        } else {
            echo "gagal login";
        }	
    }

    function logout()
    {
        $this->session->sess_destroy();

        // $this->session->set_flashdata('msg_status','success');
        // $this->session->set_flashdata('msg_color','bg-blue');
        // $this->session->set_flashdata('msg_text','Thank You!');

        redirect(base_url().'user/login');
    }
}